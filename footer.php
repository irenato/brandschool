<?php
/**
 * Дизайн школы бренда
 * Шаблон хэдера
 * https://wirgman.com
 * @package WordPress
 * @subpackage clean
 */

	wp_footer(); // Необходимо для нормальной работы плагинов
?>
	<footer class="container-fluid" id="footer">
		<div class="container" id="footer-container">
			<div class="row no-gutters justify-content-center">
				<div class="col-auto text-center">
					<img id="footer-logo" src="<?= get_option('logo') ?>" alt="">
				</div>
				<div class="col-auto text-center mt-4">
					<img src="<?= get_template_directory_uri() ?>/img/call-icon.png">
					<a id="footer-tel" href="tel:<?= filterPhone(get_option('phone')) ?>"><?= get_option('phone') ?></a>
					<p id="footer-adress">
						<img src="<?= get_template_directory_uri() ?>/img/location-icon.png">
                        <?= get_option('address') ?>
						</p>
				</div>
				<div class="col-auto text-center mt-4">
					<div id="footer-btn-div">
						<button id="footer-btn" type="button" class="js-modal">
							перезвоните мне
						</button>
					</div>
				</div>
			</div>
		</div>
	</footer>
</body>
</html>