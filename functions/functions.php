<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 14.07.18
     * Time: 17:48
     */

    function filterPhone($phone)
    {
        return str_replace(['-', '(', ')', ' '], '', $phone);
    }

    add_action('wp_ajax_nopriv_register', 'register');
    add_action('wp_ajax_register', 'register');

    function register()
    {
        global $wpdb;
        $data = [];
        foreach ($_POST['data'] as $key => $value){
            $data[$key] = esc_sql(trim($value));
        }
        $headers      = 'From: ' . get_option('admin_email') . '.' . '<' . get_option('admin_email') . '>';
        $to           = get_option('admin_email');
        $mail_subject = 'Новая заявка ' . $data['name'] . "\r\n";
        $mail_text    = 'Дата: ' .   date('d-m-Y H:i:s') . "\r\n";
        $mail_text    .= 'Контактные данные ' . $data['name'] . "\r\n";
        $mail_text    .= 'Контактный телефон: ' . $data['phone'] . "\r\n";
        if(!empty($data['price'])){
            $mail_text    .= 'Цена: ' . $data['price'] . "\r\n";
        }
        $telebot          = Gwptb_Self::get_instance();
        $telebot->token   = '570157990:AAExYIQg6JuNFAHuebIHxC-yfK85o_chzmY';
        $telebot->api_url = 'https://api.telegram.org/bot' . $telebot->token . '/';

        $chat_ids = explode(',', get_option('teplobot_chats_ids'));
        if ($chat_ids) {
            foreach ($chat_ids as $chat_id)
                $telebot->send_notification(['chat_id' => trim($chat_id), 'text' => $mail_text, 'parse_mode' => 'HTML']);
        }
        wp_mail($to, $mail_subject, $mail_text, $headers);
        wp_send_json($wpdb->insert($wpdb->prefix . 'people', $data, ''));

    }