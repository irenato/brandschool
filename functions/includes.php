<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 14.07.18
     * Time: 17:48
     */


    if (!is_admin()) {
//        wp_enqueue_script('brandschool-jquery-3.3.1.slim.min.js', get_template_directory_uri() . '/js/jquery-3.3.1.slim.min.js', false, '', true);
        wp_enqueue_script('brandschool-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', false, '', true);
        wp_enqueue_script('jquery.maskedinput-1.3.js', get_template_directory_uri() . '/js/jquery.maskedinput-1.3.js', false, '', true);
        wp_enqueue_script('brandschool-functions', get_template_directory_uri() . '/js/functions.js', ['jquery'], '', true);
        wp_localize_script('brandschool-functions', 'alevel_ajax', [
            'ajax_url' => admin_url('admin-ajax.php'),
        ]);
    }