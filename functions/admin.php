<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 14.07.18
     * Time: 21:11
     */


    function theme_settings_page()
    {

        echo '<div class="wrap">';
        echo '<h1>Настройки темы</h1>';
        echo '<form method="post" action="options.php" enctype="multipart/form-data">';
        settings_fields("section");
        do_settings_sections("theme-options");
        submit_button();
        echo '</form>';
        echo '</div>';
    }

    function display_phone_element()
    {
        echo '<input type="text" name="phone" id="phone" value="' . get_option('phone') . '"/>';
    }


    function display_address_element()
    {
        echo '<textarea cols="75" name="address" id="address" >' . get_option('address') . '</textarea>';
    }

    function display_admin_chats_ids()
    {
        echo "Для получения id нужно боту <a href='https://telegram.me/get_id_bot'>@get_id_bot</a> отправить сообщение /my_id";
        echo '<textarea cols="75" name="teplobot_chats_ids" id="teplobot_chats_ids" >' . get_option('teplobot_chats_ids') . '</textarea>';
    }

    function logo_display()
    {
        echo '<input type="file" name="logo"/>';
        echo get_option('logo');
    }

    function handle_logo_upload()
    {

        if (!empty($_FILES["logo"]["tmp_name"])) {
            $urls = wp_handle_upload($_FILES["logo"], ['test_form' => FALSE]);
            $temp = $urls["url"];
            return $temp;
        }

        return get_option('logo');
    }

    function display_theme_panel_fields()
    {
        add_settings_section("section", "", null, "theme-options");
        add_settings_field("logo", "Логотип", "logo_display", "theme-options", "section");
        add_settings_field("phone", "Контактный номер телефона", "display_phone_element", "theme-options", "section");
        add_settings_field("address", "Адрес", "display_address_element", "theme-options", "section");
        add_settings_field("teplobot_chats_ids", "Chat-id (через запятую)", "display_admin_chats_ids", "theme-options", "section");

        register_setting("section", "logo", "handle_logo_upload");
        register_setting("section", "phone");
        register_setting("section", "address");
        register_setting("section", "teplobot_chats_ids");
        register_setting("section", "teplobot_chats_ids_mc");
    }

    add_action("admin_init", "display_theme_panel_fields");

    function add_theme_menu_item()
    {
        add_menu_page("Контактные данные", "Контактные данные", "manage_options", "theme-panel", "theme_settings_page", null, 99);
    }

    add_action("admin_menu", "add_theme_menu_item");

    function add_theme_mc_hub()
    {
        add_menu_page("Заявки", "Заявки " . countNewParticipants(), "manage_options", "theme-panel_mc_hub", "manageEvents", 'dashicons-universal-access-alt', 2);
    }

    add_action("admin_menu", "add_theme_mc_hub");

    function manageEvents()
    {
        global $wpdb;
        if (isset($_POST['event'])) {
            $table            = 'wp_people';
            $fields['status'] = 1;
            $where['id']      = addslashes($_POST['event']['id']);
            $wpdb->update($table, $fields, $where);
        }

        $events = $wpdb->get_results("SELECT * FROM `wp_people` ORDER BY `id`");
        ?>

        <table class="wp-list-table widefat fixed striped pages">
            <tr>
                <th class="manage-column column-author">
                    id
                </th>
                <th class="manage-column">
                    Имя
                </th>
                <th class="manage-column">
                    Телефон
                </th>
                <th class="manage-column">
                    название кнопки
                </th>
                <th class="manage-column">
                    цена
                </th>
                <th class="manage-column">
                    действие
                </th>
            </tr>
            <?php if ($events): ?>
                <?php foreach ($events as $event) : ?>
                    <form method="post" action="/wp-admin/admin.php?page=theme-panel_mc_hub">
                        <tr>
                            <td class="manage-column column-author">
                                <input type="hidden" name="event[id]" value="<?= $event->id; ?>">
                                <?= $event->id; ?>
                            </td>
                            <td class="manage-column">
                                <?= $event->name; ?>
                            </td>
                            <td class="manage-column">
                                <?= $event->phone; ?>
                            </td>
                            <td class="manage-column">
                                <?= $event->button; ?>
                            </td>
                            <td class="manage-column">
                                <?= $event->price; ?>
                            </td>
                            <th class="manage-column">
                                <?php if ($event->status == 0): ?>
                                    <input type="submit" class="button button-primary" value="подтвердить">
                                <?php endif; ?>
                            </th>
                        </tr>
                    </form>
                <?php endforeach; ?>
            <?php endif; ?>
        </table>

        <?php
    }

    function allParticipants()
    {
        global $wpdb;
        $events = $wpdb->get_results("SELECT * FROM `wp_event_participants` ORDER BY `id` DESC");
        ?>
        <table class="wp-list-table widefat fixed striped pages">
            <tr>
                <th class="manage-column column-author">
                    id
                </th>
                <th class="manage-column">
                    Имя
                </th>
                <th class="manage-column">
                    Телефон
                </th>
                <th class="manage-column">
                    название кнопки
                </th>
                <th class="manage-column">
                    цена
                </th>
                <th class="manage-column">
                    действие
                </th>
            </tr>
            <?php if ($events): ?>
                <?php foreach ($events as $event) : ?>
                    <form method="post" action="/wp-admin/admin.php?page=my-custom-submenu-page991">
                        <tr>
                            <td class="manage-column column-author">
                                <input type="hidden" name="event[id]" value="<?= $event->id; ?>">
                                <?= $event->id; ?>
                            </td>
                            <td class="manage-column">
                                <?= $event->name; ?>
                            </td>
                            <td class="manage-column">
                                <?= $event->phone; ?>
                            </td>
                            <td class="manage-column">
                                <?= $event->button; ?>
                            </td>
                            <td class="manage-column">
                                <?= $event->price; ?>
                            </td>
                            <th class="manage-column">
                                <input type="submit" class="button button-primary" value="обновить">
                            </th>
                        </tr>
                    </form>
                <?php endforeach; ?>
            <?php endif; ?>
        </table>
        <?php

    }

    function countNewParticipants()
    {
        global $wpdb;
        $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_people WHERE status=0;");
        if ($count_applications) {
            return ' (' . $count_applications . ')';
        }
    }



