<?php
    /**
     * Чистый Шаблон для разработки
     * Функции шаблона
     * http://dontforget.pro
     * @package    WordPress
     * @subpackage clean
     */
    include('functions/includes.php');
    include('functions/functions.php');

    if (is_admin()) {
        include('functions/admin.php');
    }

    register_nav_menus([ // Регистрируем 2 меню
        'top'  => 'Верхнее меню',
        'left' => 'Нижнее',
    ]);
    add_theme_support('post-thumbnails'); // Включаем поддержку миниатюр
    set_post_thumbnail_size(254, 190); // Задаем размеры миниатюре

    if (function_exists('register_sidebar')) {
        register_sidebar();
    } // Регистрируем сайдбар

?>