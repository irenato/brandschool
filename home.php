<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 14.07.18
     * Time: 18:34
     */

    /**
     * Template name: Home
     */

    get_header();
    if (have_posts()) : while (have_posts()) : the_post();
        ?>

        <div class="container-fluid" id="block-1">
            <div class="container" id="block-1-container">
                <nav class="navbar navbar-expand-md no-gutters" id="navigation">
                    <div class="col-12 col-sm col-lg-4 col-xl-5 d-flex align-items-center justify-content-between">
                        <img src="<?= get_option('logo') ?>" alt="" id="logo" class="navbar-brand">
                        <button id="navBtn" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                                aria-label="toggle-navigation">
                            <i class="fas fa-bars"></i>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse d-md-flex justify-content-end" id="navbarSupportedContent">
                        <ul class="navbar-nav d-sm-inline-flex flex-sm-row align-items-center justify-content-between">
                            <li class="navbar-item mt-3 mt-sm-0">
                                <img src="/wp-content/themes/brandschool/img/call-icon.png" alt="">
                                <a href="tel:<?= filterPhone(get_option('phone')) ?>"><?= get_option('phone') ?></a>
                            </li>
                            <li class="navbar-item my-3">
                                <div id="button_back">
                                    <button id="back_call" class="js-modal">
                                        обратный звонок
                                    </button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="row no-gutters" id="row-1">
                    <div class="col-auto" id="for-title">
                        <p id="small-header"><?= get_field('type') ?></p>
                        <h1 id="title"><?= get_field('name') ?></h1>
                        <p id="small-header-2"><?= get_field('dates') ?></p>
                        <img src="/wp-content/themes/brandschool/img/dots.png" alt="" id="dots">
                        <img src="/wp-content/themes/brandschool/img/small-circle.png" alt="" id="circle1">
                        <img src="/wp-content/themes/brandschool/img/small-circle.png" alt="" id="circle2">
                        <img class="d-none d-md-inline-block" src="/wp-content/themes/brandschool/img/small-circle.png" alt="" id="circle3">
                        <img class="d-none d-md-inline-block" id="waves-1" src="/wp-content/themes/brandschool/img/waves.png">
                    </div>
                </div>
                <div class="row no-gutters align-items-center" id="row-2">
                    <div class="col col-sm-auto">
                        <p id="description"><?= get_field('discount_description') ?></p>
                    </div>
                    <div class="col col-sm-auto">
                        <p class="my-auto" id="procents"><?= get_field('discount') ?>%</p>
                        <img class="d-none" src="/wp-content/themes/brandschool/img/small-circle.png" alt="">
                    </div>
                </div>
                <div class="row no-gutters" id="row-3">
                    <div class="col col-md-auto text-center align-self-start my-4" id="for-intensive-btn">
                        <div id="intensive-div">
                            <button id="intensive-btn" class="popmake-22 js-modal" type="button">
                                записаться на интенсив
                            </button>
                        </div>
                        <img class="d-none d-md-inline-block" src="/wp-content/themes/brandschool/./img/dots-triangle.png" alt="">
                    </div>
                    <div class="col-12 col-md order-first order-md-0 d-flex align-items-start align-items-md-center">
                        <img class="d-md-none" src="/wp-content/themes/brandschool/img/waves.png" alt="" id="waves">
                        <div id="timer">
                            <p id="timer-header">цена станет выше через</p>
                            <p id="nums">02 : 02 : 20</p>
                            <span id="month">месяца</span>
                            <span id="day">дня</span>
                            <span id="minute">минут</span>
                        </div>
                    </div>
                    <img src="/wp-content/themes/brandschool/img/woman.png" alt="" id="woman-with-chair-pic" class="d-none d-lg-block">
                </div>
            </div>
            <img id="dots-waves-img" src="/wp-content/themes/brandschool/img/dots_and_waves.png">
        </div>

        <div class="container-fluid" id="block-2">
            <div class="container" id="block-2-container">
                <div class="row align-items-start" id="block-2-row">
                    <?php if ($parners = get_field('organizators')): ?>
                        <div class="col-auto col-sm col-lg-auto">
                            <p class="partners-title">Организаторы:</p>
                            <?php foreach ($parners as $partner): ?>
                                <img class="partners-logo d-block d-sm-inline-block" src="<?= $partner['logo'] ?>" id="nataly-pic">
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                    <?php if ($parners = get_field('partners')): ?>
                        <div class="col-auto col-sm">
                            <p class="partners-title">Наши партнёры:</p>
                            <?php foreach ($parners as $partner): ?>
                                <img class="partners-logo d-block d-sm-inline-block" src="<?= $partner['logo'] ?>" alt="A-LEVEL" id="alevel-pic">
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="container-fluid" id="block-3">
            <div class="container" id="block-3-container">
                <?= get_the_content() ?>
            </div>
            <img id="three-lines" src="/wp-content/themes/brandschool/img/three-lines.png">
        </div>

        <?php if ($items = get_field('for_whom')): ?>
            <div class="container-fluid" id="block-4">
                <div class="container" id="block-4-container">
                    <div class="row no-gutters">
                        <div id="for-header-4">
                            <h2 id="block-4-header">
                                Для кого этот интесив?</h2>
                            <img id="header-dots-2" src="/wp-content/themes/brandschool/img/dots-triangle-2.png">
                            <img id="yellow-circle-2" src="/wp-content/themes/brandschool/img/yellow-circle.png" alt="">
                            <img id="purple-circle" class="d-none" src="/wp-content/themes/brandschool/img/purple-circle.png">
                        </div>
                    </div>

                    <?php foreach ($items as $item): ?>
                        <div class="row no-gutters">
                            <div class="col-12 for-text-4">
                                <div class="row">
                                    <div class="col-12 text-center col-xs-auto">
                                        <img src="<?= $item['image1'] ?>" alt="<?= $item['title1'] ?>" class="block-4-logo">
                                    </div>
                                    <div class="col">
                                        <h3 class="block-4-text-header"><?= $item['title1'] ?></h3>
                                        <p class="block-4-text"><?= $item['description1'] ?> </p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 for-text-4">
                                <div class="row">
                                    <div class="col-12 text-center col-xs-auto">
                                        <img src="<?= $item['image1'] ?>" alt="<?= $item['title2'] ?>" class="block-4-logo">
                                    </div>
                                    <div class="col">
                                        <h3 class="block-4-text-header"><?= $item['title2'] ?></h3>
                                        <p class="block-4-text"><?= $item['description2'] ?> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <img id="purple-dots-circle" src="/wp-content/themes/brandschool/img/purple-dots-circle.png">
            </div>
        <?php endif; ?>

        <div class="container-fluid" id="block-5">
            <div class="container" id="block-5-container">
                <div class="row no-gutters">
                    <div id="for-header-5">
                        <h2 id="block-5-header">
                            <?= get_field('form1_title') ?>
                        </h2>
                        <img id="header-5-dots" src="/wp-content/themes/brandschool/img/dots.png" alt="">
                        <img id="header-5-circle-1" src="/wp-content/themes/brandschool/img/small-circle.png" alt="">
                        <img id="header-5-circle-2" src="/wp-content/themes/brandschool/img/small-circle.png" alt="">
                    </div>
                    <p id="block-5-description">
                        <?= get_field('form1_description') ?></p>
                </div>
                <div class="row no-gutters">
                    <?php include('template-parts/form.php'); ?>
                </div>
                <div class="row no-gutters">
                    <div class="col-12 text-center align-self-end" id="for-intensive-btn-2">
                        <div id="intensive-div-2">
                            <button id="intensive-btn-2" class="js-submit" type="button">
                                записаться на интенсив
                            </button>
                        </div>
                        <img class="d-none" src="/wp-content/themes/brandschool/./img/dots-triangle.png" alt="">
                    </div>
                    <div class="col-12 order-first d-flex align-items-start">
                        <div id="timer-2">
                            <p id="timer-header-2">цена станет выше через</p>
                            <p id="nums-2">02 : 02 : 20</p>
                            <span id="month-2">месяца</span>
                            <span id="day-2">дня</span>
                            <span id="minute-2">минут</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($items = get_field('course_program_details')): ?>
            <div class="container-fluid" id="block-6">
                <div class="container" id="block-6-container">
                    <div class="row no-gutters">
                        <div class="col-12" id="intensive-prog">
                            <?= get_field('course_program') ?>
                        </div>
                        <div class="col-12" id="modules-list">
                            <?php $i = 0;
                                foreach ($items as $item): ?>
                                    <div class="row no-gutters">
                                        <div class="col-12 d-flex justify-content-center col-xs-auto">
                                            <p class="modules-num">
                                                <span class="modules-digit"><?= ++$i < 10 ? ('0' . $i) : $i ?>.</span>
                                                <br>модуль</p>
                                        </div>
                                        <div class="col">
                                            <p class="modules-header"><?= $item['title'] ?></p>
                                            <p class="modules-text">
                                                <?= $item['description'] ?>
                                            <p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>

                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if ($organisators_items = get_field('organisators_items')): ?>
            <div class="container-fluid" id="block-7">
                <img id="block-7-big-circle" src="/wp-content/themes/brandschool/img/big-circle-2.png">
                <div class="container" id="block-7-container">
                    <div class="row no-gutters">
                        <div id="for-header-7">
                            <h2 id="block-7-header"><?= get_field('organisators_title') ?></h2>
                            <img id="header-7-dots" src="/wp-content/themes/brandschool/img/dots.png">
                            <img id="header-7-circle" src="/wp-content/themes/brandschool/img/yellow-circle.png">
                        </div>
                        <div id="for-description-7">
                            <p id="block-7-description">
                                <?= get_field('organisators_description') ?></p>
                            <img id="description-7-waves" src="/wp-content/themes/brandschool/img/waves.png">
                        </div>
                    </div>
                    <div class="row justify-content-center no-gutters" id="treners-container">
                        <?php foreach ($organisators_items as $item): ?>
                            <div class="col-auto col-xs-6">
                                <img class="treners-logo" src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>">
                                <h3 class="treners-header"><?= $item['title'] ?></h3>
                                <p class="treners-description"><?= $item['description'] ?></p>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <img id="block-7-lines" src="/wp-content/themes/brandschool/img/three-lines.png">
            </div>
        <?php endif; ?>
        <?php if ($formats = get_field('formats')): ?>
            <div class="container-fluid" id="block-8">
                <div class="container" id="block-8-container">
                    <div class="row no-gutters">
                        <div id="for-header-8">
                            <h2 id="block-8-header"><?= get_field('format_title') ?></h2>
                            <img id="header-8-yellow-circle" src="/wp-content/themes/brandschool/img/yellow-circle.png">
                            <img id="header-8-triangle" src="/wp-content/themes/brandschool/img/dots-triangle-2.png" alt="">
                        </div>
                    </div>
                    <div class="row no-gutters justify-content-center">
                        <?php foreach ($formats as $item): ?>
                        <div class="col-auto price-container">
                            <img class="rectangle-1" src="/wp-content/themes/brandschool/img/yellow-rectangle.png">
                            <img class="rectangle-2" src="/wp-content/themes/brandschool/img/yellow-rectangle.png">
                            <div class="price-div">
                                <h3 class="price-header"><?= $item['title'] ?></h3>
                                <?= $item['description'] ?>
                                <p class="price-cost">
                                    <span><?= $item['price_old'] ?></span> <?= $item['price'] ?>
                                </p>
                                <div class="price-button-div">
                                    <button class="price-button js-modal" data-price="<?= $item['title'] ?>">
                                        ПРИНЯТЬ УЧАСТИЕ
                                    </button>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="row no-gutters">
                        <p id="block-8-text"><?= get_field('format_description') ?></p>
                    </div>
                </div>
                <img id="block-8-purple-circle" src="/wp-content/themes/brandschool/img/purple-dots-circle.png">
            </div>
        <?php endif; ?>
        <div class="container-fluid" id="block-9">
            <div class="container" id="block-9-container">
                <div class="row no-gutters">
                    <div id="for-header-9">
                        <h2 id="block-9-header">
                            <?= get_field('form2_title') ?>
                        </h2>
                        <img id="header-9-dots" src="/wp-content/themes/brandschool/img/dots.png" alt="">
                        <img id="header-9-circle-1" src="/wp-content/themes/brandschool/img/small-circle.png" alt="">
                    </div>
                    <p id="block-9-description">
                        <?= get_field('form2_description') ?></p>
                </div>
                <div class="row no-gutters">
                    <?php include('template-parts/form.php'); ?>
                </div>
                <div class="row no-gutters">
                    <div class="col-12 text-center align-self-end" id="for-intensive-btn-3">
                        <div id="intensive-div-3">
                            <button id="intensive-btn-3" class="js-submit" type="button">
                                записаться на интенсив
                            </button>
                        </div>
                        <img class="d-none" src="/wp-content/themes/brandschool/./img/dots-triangle.png" alt="">
                    </div>
                    <div class="col-12 order-first d-flex align-items-start">
                        <div id="timer-3">
                            <p id="timer-header-3">цена станет выше через</p>
                            <p id="nums-3">02 : 02 : 20</p>
                            <span id="month-3">месяца</span>
                            <span id="day-3">дня</span>
                            <span id="minute-3">минут</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php

        include('template-parts/register-modal.php');

    endwhile;
    endif;

    get_footer();
