<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 14.07.18
     * Time: 22:34
     */

?>

<form  action="/" method="post" class="js-ajax">
    <label for="nameInp2">Как вас зовут</label>
    <input name="data[name]" type="text" placeholder="Имя" required="required">
    <label for="phoneInp2">Ваш телефон</label>
    <input name="data[phone]" type="text" placeholder="+38(___)___-__-__" required="required">
    <input type="hidden" name="data[button]" value="записаться на интенсив">
    <input type="hidden" name="action" value="register">

    <div class="js-message" style="display: none">
        Ваша заявка создана успешно! Мы обязательно свяжемся с Вами.
    </div>
</form>
