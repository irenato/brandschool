<?php
    /**
     * Created by PhpStorm.
     * User: macbook
     * Date: 14.07.18
     * Time: 18:14
     */

?>


<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background-color: #642952; color: #ffffff">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Оставить заявку</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="js-ajax" action="/" method="post" style="background-color: ">
                <div class="modal-body">
                    <label for="name">Как Вас зовут</label>
                    <input type="text" id="name" name="data[name]" value="" required>
                </div>
                <div class="modal-body">
                    <label for="phone">Ваш телефон</label>
                    <input type="text" id="phone" name="data[phone]" value="" required>
                </div>
                <div class="modal-body js-message" style="display: none">
                    Ваша заявка создана успешно! Мы обязательно свяжемся с Вами.
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="data[button]" value="">
                    <input type="hidden" name="data[price]" value="">
                    <input type="hidden" name="action" value="register">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary">Отправить заявку</button>
                </div>
            </form>
        </div>
    </div>
</div>

