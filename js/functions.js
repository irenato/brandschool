/**
 * Created by macbook on 14.07.18.
 */
(function ($) {
    $(document).ready(function () {
        $('.js-modal').on('click', function () {
            var modal = $('#register-modal');
            $(modal).find("input[name='data[button]']").val($(this).text());
            $(modal).find("input[name='data[price]']").val((typeof $(this).data('price') !== 'undefined' ? $(this).data('price') : ''));
            $(modal).find("[type='submit']").attr('disabled', false);
            $(modal).modal('show');
        })

        $("input[name='data[phone]']").mask("+38?(999) 999-99-99");

        $('.js-ajax').on('submit', function () {
            var _this = $(this);
            $(_this).find("[type='submit']").attr('disabled', 'disabled');
            $(_this).find('.js-message').slideToggle();
            $.ajax({
                url    : '/wp-admin/admin-ajax.php',
                type   : 'POST',
                data   : $(_this).serialize(),
                success: function (data) {
                    $(_this)[0].reset();
                    setTimeout(function () {
                        $('.js-message').hide();
                        $('#register-modal').modal('hide'), 2000
                    });
                }
            });
            return false;
        })

        $('.js-submit').on('click', function () {
            $(this).closest('div.container').find('form').trigger('submit');
        })
    });
})(jQuery);